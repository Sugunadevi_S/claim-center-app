const autoReport = require("../controllers/autoReport.controller");

module.exports = (app) => {
  app.get(
    "/getJsonResponse",
    autoReport.getJsonData
  );
  app.post(
    "/insertData",
    autoReport.insertData
  );
}