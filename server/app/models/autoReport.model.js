const pool = require("./db");
const mssql = require('mssql');
const util = require("util");
const xml2js = require('xml2js');
const fsPromises = require('fs').promises;
const path = require('path');

exports.InsertReportData = (insertValues) =>{
    pool.connect()
        .then(() => {
            console.log('Connected to MSSQL database');
            insertValues.map((data) => {
                const req = new mssql.Request(pool);
                let sql = "INSERT INTO claimsCenterReport (TC, Status, Time, Type) VALUES ";
                sql += util.format("('%s', '%s', '%f', '%s')", data.TestCase, data.Status, data.Time, data.Type);
                req.query(sql)
                    .then(result => {
                        return result;
                    })
                    .catch(err => {
                        console.error('Error inserting record:', err);
                    })
            })
        })
        .catch((err) => {
            console.error('Error connecting to MSSQL database:', err);
        });
}


function getLatestFile(files) {
    let latestDate = null;
    let latestFile = null;

    files.forEach((file) => {
        const match = file.match(/(\d{2})(\d{2})(\d{4})/); // Assuming the date format is DDMMYYYY
        if (match) {
            const day = parseInt(match[1], 10);
            const month = parseInt(match[2], 10) - 1; // Subtract 1 from the month as it's zero-based
            const year = parseInt(match[3], 10);

            const fileDate = new Date(year, month, day);

            if (!latestDate || fileDate > latestDate) {
                latestDate = fileDate;
                latestFile = file;
            }
        }
    });

    return latestFile;
}

exports.getData = async () => {
    try {
        const folderPath = "//pvspta01/ToscaCI/reports/Regression smoke packs/Claims/ClaimCenter"

    
    const filteredFiles = [];
    
        try {
            const extractFile = await fsPromises.readdir(folderPath);
            const latestFile = await getLatestFile(extractFile);
            if (latestFile) {
            const filePath = path.join(folderPath, latestFile);
            filteredFiles.push(filePath);
            } else {
            console.log("No valid files found in the directory.");
            }
        } catch (error) {
            console.error(`Error reading files from folder ${folderPath}:`, error);
        }
        
    if (filteredFiles.length === 0) {
            return "No files found for the given date.";
        }

        const fileDataPromises = filteredFiles.map(async (dataFile) => {
        const xmlData = await fsPromises.readFile(dataFile, 'utf8');
        return xml2js.parseStringPromise(xmlData);
    });
    
    const jsonResults = await Promise.all(fileDataPromises);
    const result = jsonResults.map((result) => result.testsuites.testsuite);
    return result;
    } catch (error) {
        return error;
        
    }
};