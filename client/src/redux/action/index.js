import config from '../../config';
import axios from 'axios';

export const FETCH_JSON_RESPONSE = 'FETCH_JSON_RESPONSE';


/** getting json reponse from all the xml files */
export const getXmltoJsonResponse = () => async (dispatch) => {
  try {
    const response = await axios.get(`${config.serviceUrl}${config.getJsonResponse}`);
    dispatch({
      type: FETCH_JSON_RESPONSE,
      payload: response.data.result
    });
    console.log("response.data.result", response.data.result);
    return response.data.result;
  } catch (error) {
    console.error('Error:', error);
  }
};

/** Save the extracted data to the database */
export const saveExtractedData = (value) => async (dispatch) => {
  let body =  JSON.stringify(value);
  axios
  .post(config.serviceUrl + config.insertData, body, {
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => res.data.result).catch((err) => err.response?.data);
}

