import { combineReducers } from 'redux';
import { FETCH_JSON_RESPONSE } from '../action/index';

// Define the initial state for this reducer
const initialState = {
  jsonResponse: {}
};

// Define the reducer function
const folderNamesReducer = (state = initialState, action) => {
  switch (action.type) {
      case FETCH_JSON_RESPONSE:
        return{
          ...state,
          jsonResponse: action.payload,
        };
    default:
      return state;
  }
};

// Export the reducer using combineReducers
export default combineReducers({
  folderNames: folderNamesReducer,
});
