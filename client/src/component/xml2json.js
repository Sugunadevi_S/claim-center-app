import React from 'react';
import { useDispatch} from 'react-redux';
import {getXmltoJsonResponse, saveExtractedData} from '../redux/action/index';
import XLSX from 'sheetjs-style';


const XMLDisplay = () => {
  
  const dispatch = useDispatch();

  /* Date Formatting Function */
  const ValidateDate = (data) => {
    if (data < 10) {
      data = "0" + data;
    }
    return data;
  };

  /** once we got extracted data this function will generate the excel report */
  const generateExcel = (extractData) => {
    if (extractData.length === 0) return "Failure";
  
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.json_to_sheet(extractData);
  
    XLSX.utils.book_append_sheet(workbook, worksheet, "AutomaticReport");
  
    const today = new Date();
    const date = `${ValidateDate(today.getDate())}${ValidateDate(today.getMonth() + 1)}${today.getFullYear()}`;
  
    const fileName = `Claim Center Report - ${date}.xlsx`;
  
    XLSX.writeFile(workbook, fileName);
    return "Success";
  };

  /** make the excel formatted data from json response */
  const exportToExcel = async (exportData) => {
    if (exportData.length === 0) {
        return "Failure";
    }

    const flattenedData = exportData.flatMap(items => items.map(item => item.testcase));

    const transformItem = item => {
       const extractData = item.map((data) => {
        const rawTime = parseFloat(data.$.time);
        const roundedTime = rawTime.toFixed(2);
        if (data.failure) {
          //console.log("data", data.$.name);
          return {
            TestCase: data.$.name,
            Status: "Failed",
            Time: roundedTime,
            Type: type.toString()
          };
        } 
      else {
        return {
          TestCase: data.$.name,
          Status: "Passed",
          Time: roundedTime,
          Type: type.toString()
        };
      }
     });
     return extractData;
    };
    
    const type = exportData.map(data => {
      return data[0].$.name.split("_")[1];
    })
    const splittedItems = flattenedData.map(transformItem).flat(); 
    const finalArray = splittedItems;
    console.log("finalArray", finalArray);
    
    await dispatch(saveExtractedData(finalArray));
    const generatedResult = generateExcel(finalArray);
    const result = (generatedResult === "Success") ? "Success" : "Failure";
    return result;
  };    

  const handleExport = async() => {
    const jsonResponse = await dispatch(getXmltoJsonResponse());
    const convertedData = await exportToExcel(jsonResponse);
    console.log("convertedData", convertedData);
  };
    
  return <><button className="btn btn-success m-3" onClick={handleExport}>Export to Excel</button></>
};

export default XMLDisplay;