import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import XMLDisplay from './component/xml2json';

function App() {

  return (
    <div className="App">
      <div>
        <XMLDisplay />
      </div>  
    </div>
  );
}

export default App;
